# Klima Alarm
Audible alert circuit to be connected to the temperature displays inside the electrical cabinet in the control room.

![Alt text](Images/case.jpg?raw=true "PCB fits Phoenix Contact 53,6mm BC Series case ")

## Specifications
* Operating voltage: 9..24V DC 
* Power consumption: t.b.d.
* 8 digital inputs
	* Active high inputs
	* Switching level: min. 2V
	* Input impedance: ~6kOhm
* 2 open collector outputs, max. current 60mA
* Operating conditions: t.b.d.
* Accustic annunciator: piezo electric transducer, 85dbA, 2300Hz
* Display: 7-segment, red
* Case: DIN rail, Phoenix Contact 53,6mm BC Series

## Working principle
The circuit comprises 8 active high digital inputs. The inputs are diode clamped at about 3,5V.
The clamping voltage is derived via D1 and D3. D3 is positively biased to quickly conduct any transient
bursts on the inputs to safety ground. The input resistors R4-R7 and R20-R23 are fusible and provide
a safety moat and prevent flashover to the rest of the circuit. The safety ground must be connected to 
earth ground. Shields of incoming cables must be connected to safety ground. 

5V system supply voltage is provided via a 7805 type linear regulator. Input protection is formed by
F1, D13 and D14. FL1 suppresses common mode noise getting in/out of the circuit.

Two open collector outputs are turned on during alarm condition. Both outputs are designed to sink
approximately 60mA via simple transistor current limiting. The outputs are referenced to system ground.

All logic functions are handled by a PIC16F15355 microcontroller. In circuit programming is done via
J1 (6 pin female header).

### Software
After initialization, inside main loop, all 8 inputs are scanned indefinitely. Every time a logic high
level on any of the inputs is detected, the corresponding input channel number is written to the alarm
register and also shown on the 7-segment-display.
As long as the alarm register does not equal zero, the buzzer is turned on. During alarm condition,
each button press cycles through the pending alarms on the display.
To reset the alarm register, the push-button needs to be held down for at least 2 seconds.
Alarm buzzer and LED segments can be tested during non-alarm condition by pressing and holding the push-button
for at least 2 seconds.

## Schematic diagram
![Alt text](Images/schematic.png?raw=true "Schematic")

## PCB Layout
![Alt text](Images/top.png?raw=true "Top")
![Alt text](Images/bottom.png?raw=true "Bottom")

## Firmware and programming
The firmware is written in C with MPLABX and XC8 compiler. No additional tools or libraries are required.
Firmware flashing is done via Microchip ICD or PICkit.