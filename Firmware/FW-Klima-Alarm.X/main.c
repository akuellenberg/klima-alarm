/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC16LF15356
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"
#include "segment.h"
#include "alarm.h"

volatile uint8_t gTimeout = 0;

void isrTimeout(void) 
{
    gTimeout = 1;
}

/*
                         Main application
 */
void main(void)
{
    uint8_t state = 0;
    uint8_t key;

    // initialize the device
    SYSTEM_Initialize();
    
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
   
    initInputs();
    clearAlarms();
    
    BUZZER_LAT = 1;    
    setDigit(8); 
    __delay_ms(800);
    
    BUZZER_LAT = 0;
    setDigit(10);
    
    TMR0_SetInterruptHandler(isrDebounceInputs);

    TMR1_SetTickerFactor(5);
    TMR1_SetInterruptHandler(isrBlinkingDot);
  
    TMR2_SetInterruptHandler(isrTimeout);
    TMR2_Stop();
    
        
    while (1)
    {
        switch(state) {
            case 0:
                if (checkAlarm()) {
                    state = 1;
                    SEG_DP_LAT = 1;
                    BUZZER_LAT = 1;
                    OUT1_LAT = 1;
                    OUT2_LAT = 1;
                    TMR1_SetTickerFactor(10);
                    TMR1_SetInterruptHandler(isrDisplayAlarms);
                }
                break;
            case 1:
                key = readKey();
                if (key == 1) {
                    clearAlarms();
                    BUZZER_LAT = 0;
                    OUT1_LAT = 0;
                    OUT2_LAT = 0;
                    setDigit(10);
                    TMR1_SetTickerFactor(5);
                    TMR1_SetInterruptHandler(isrBlinkingDot);
                    state = 0;
                } else if (key == 3) {
                    BUZZER_LAT = 0;
                    OUT1_LAT = 0;
                    OUT2_LAT = 0;
                    clearAlarms();
                    setDigit(10);
                    TMR1_SetTickerFactor(1);
                    TMR1_SetInterruptHandler(isrMutePattern);
                    TMR2_Start();
                    state = 2;
                }
                break;
            case 2:
                key = readKey();
                if ((key == 3) || gTimeout) {
                    setDigit(10);
                    TMR1_SetTickerFactor(5);
                    TMR1_SetInterruptHandler(isrBlinkingDot);
                    TMR2_Stop();
                    gTimeout = 0;
                    state = 0;
                }
                break;
            default:
                BUZZER_LAT = 0;
                OUT1_LAT = 0;
                OUT2_LAT = 0;
                setDigit(10);
                TMR1_SetTickerFactor(5);
                TMR1_SetInterruptHandler(isrBlinkingDot);
                state = 0;
        }
    }
}
/**
 End of File
*/