#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "segment.h"

/*
 *    a
 *   ---
 * f|   |b
 *  | g |
 *   ---  
 *  |   |
 * e|   |c
 *   ---
 *    d
 */

// Mapping for 7 segment display
const uint8_t segMap[12][8] = {	
//   b a f g . c d e
	{0,0,0,1,1,0,0,0},	// 0
	{0,1,1,1,1,0,1,1},	// 1
	{0,0,1,0,1,1,0,0},	// 2
	{0,0,1,0,1,0,0,1},	// 3
	{0,1,0,0,1,0,1,1},	// 4
	{1,0,0,0,1,0,0,1},	// 5
	{1,0,0,0,1,0,0,0},	// 6
	{0,0,1,1,1,0,1,1},	// 7
	{0,0,0,0,1,0,0,0},	// 8
	{0,0,0,0,1,0,0,1},	// 9
	{1,1,1,1,1,1,1,1},	// blank
    {1,1,1,0,1,1,1,1}   // -
};

const uint8_t mutePattern2[14][8] = {
//   b a f g . c d e    
    {0,1,1,1,0,1,1,1},
    {0,0,1,1,0,1,1,1},
    {0,0,0,1,0,1,1,1},
    {0,0,0,0,0,1,1,1},
    {1,0,0,0,0,1,1,1},
    {1,1,0,0,0,1,1,1},
    {1,1,1,0,0,1,1,1},    
    {1,1,1,0,0,1,1,0},
    {1,1,1,0,0,1,0,0},
    {1,1,1,0,0,0,0,0},
    {1,1,1,1,0,0,0,0},
    {1,1,1,1,0,0,0,1},
    {1,1,1,1,0,0,1,1},
    {1,1,1,1,0,1,1,1}
};


const uint8_t mutePattern[16][8] = {
    {1,1,1,1,0,1,0,1},
    {1,1,1,1,0,0,0,1},
    {1,1,1,0,0,0,0,1},
    {1,1,1,0,0,0,0,0},
    {1,1,1,0,0,0,1,0},
    {1,1,1,0,0,1,1,0},
    {1,1,1,1,0,1,1,0},
    {1,1,1,1,0,1,1,1},

    {1,1,1,0,0,1,1,1},
    {0,1,1,0,0,1,1,1},
    {0,0,1,0,0,1,1,1},
    {0,0,0,0,0,1,1,1},
    {0,0,0,1,0,1,1,1},
    {1,0,0,1,0,1,1,1},
    {1,1,0,1,0,1,1,1},
    {1,1,1,1,0,1,1,1},
};

void setDigit(uint8_t d)
{
    digit = d;
   	SEG_A_LAT = segMap[d][1];
	SEG_B_LAT = segMap[d][0];
	SEG_C_LAT = segMap[d][5];
	SEG_D_LAT = segMap[d][6];
	SEG_E_LAT = segMap[d][7];
	SEG_F_LAT = segMap[d][2];
	SEG_G_LAT = segMap[d][3];    
}

void isrBlinkingDot(void)
{
    SEG_DP_LAT = ~SEG_DP_LAT;
}

void isrMutePattern(void)
{
    static uint8_t i = 0;

    SEG_A_LAT = mutePattern2[i][1];
	SEG_B_LAT = mutePattern2[i][0];
	SEG_C_LAT = mutePattern2[i][5];
	SEG_D_LAT = mutePattern2[i][6];
	SEG_E_LAT = mutePattern2[i][7];
	SEG_F_LAT = mutePattern2[i][2];
	SEG_G_LAT = mutePattern2[i][3]; 
    
    if (++i > 13) i = 0;
}