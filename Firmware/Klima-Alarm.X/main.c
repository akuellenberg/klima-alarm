/* 
 * File:   main.c
 * Author: Andr� K�llenberg
 *
 * Created on 12. Dezember 2019, 09:55
 */
	
#define _XTAL_FREQ 16000000L	// Internal clock freq. Required by __delay_ms()

#include "config.h"				// Configuration bits
#include <stdint.h>
#include <xc.h>

#define OUTPUT		0
#define INPUT		1

#define T_INPUTS	TRISA
#define T_BUZZER	TRISBbits.TRISB0
#define T_BUTTON	TRISBbits.TRISB1
#define T_OC2_OUT	TRISBbits.TRISB2
#define T_OC1_OUT	TRISBbits.TRISB5
#define T_SEGMENTS	TRISC

#define INPUTS		PORTA
#define BUZZER		LATBbits.LATB0
#define BUTTON		PORTBbits.RB1
#define OC2_OUT		LATBbits.LATB2 
#define OC1_OUT		LATBbits.LATB5
#define SEGMENTS	LATC
#define SEGM_A		LATCbits.LATC1
#define SEGM_B		LATCbits.LATC0
#define SEGM_C		LATCbits.LATC5
#define SEGM_D		LATCbits.LATC6
#define SEGM_E		LATCbits.LATC7
#define SEGM_F		LATCbits.LATC2
#define SEGM_G		LATCbits.LATC3
#define SEGM_DP		LATCbits.LATC4


/*
 * Global data
 */
const uint8_t g_map[11][8] = {	// Mapping for 7 segment display
//   B A F G . C D E
	{0,0,0,1,1,0,0,0},	// 0
	{0,1,1,1,1,0,1,1},	// 1
	{0,0,1,0,1,1,0,0},	// 2
	{0,0,1,0,1,0,0,1},	// 3
	{0,1,0,0,1,0,1,1},	// 4
	{1,0,0,0,1,0,0,1},	// 5
	{1,0,0,0,1,0,0,0},	// 6
	{0,0,1,1,1,0,1,1},	// 7
	{0,0,0,0,1,0,0,0},	// 8
	{0,0,0,0,1,0,0,1},	// 9
	{1,1,1,1,1,1,1,1}	// blank
};

uint8_t volatile g_digit = 10;	// Number on 7 segment display
uint8_t volatile g_btn = 0;		// State of the push button
uint8_t volatile g_dot = 0;		// State of the blinking dot indicator
uint8_t g_alarms = 0;			// Pending alarms

/*
 * Function prototypes
 */
void init(void);
void showDigit(void);
void scanInputs(void);
void LEDtest(void);
void buzzer(void);

/*
 * Main program
 */
int main(int argc, char** argv) {
	uint8_t n;
	init();
	
	g_alarms = 0;
	g_digit = 10;
	   
	while(1) {
		CLRWDT();
		scanInputs();
		
		if (g_alarms) {
			OC1_OUT = 1;				// Enable OC output 1
			
			buzzer();
			
			if (g_btn == 1) {			// Button cycles through pending g_alarms
				if (++n > 7) n = 0;
				while(!(g_alarms & (1 << n))) n++;
				g_digit = n;
			} else if (g_btn == 2) {	// Hold and release button to clear g_alarms
				g_alarms = 0;
				g_digit = 10;
				OC1_OUT = 0;			// Disable OC output 1
			}
			g_btn = 0;
		} else {
			if (g_btn > 1) {
				LEDtest();
			}
			g_btn = 0;
		}
	}
}

/*
 * Interrupt service routine
 */
void __interrupt() isr(void) {
	uint8_t static tick = 0;
	uint8_t static state = 0;
	uint8_t static btnTime = 0;
	uint8_t static dotTime = 0;
	
	// Interrupt triggered by Timer 0?
	if (PIR0bits.TMR0IF && PIE0bits.TMR0IE) {
		PIR0bits.TMR0IF = 0;		// Reset flag register

		if (++tick == 0) {
			SEGMENTS = 0xFF;		// LED dimming, all segments off
			++btnTime;				// Slow counting button timer
			if (++dotTime == 60) {	// Blinking g_dot as running indicator
				dotTime = 0;
				if (g_dot) g_dot = 0;
				else g_dot = 1;
			}
		} else if (tick == 240) {
			showDigit();			// Segments on
		}

		/* Button de-bouncing
		 * g_btn = 0 : idle state
		 * g_btn = 1 : short button push
		 * g_btn = 2 : long button push
		 * g_btn = 3 : long button push, no release 
		 * 
		 * g_btn has to be cleared manually
		 */		  
		switch(state) {
			case 0:
				if (!BUTTON) {			// Pushbutton pressed?
					state = 1;
					btnTime = 0;
				}
				break;
			case 1:
				if (btnTime > 60) {		// Not released after "long" time
					g_btn = 3;
				}
				if (BUTTON) {			// Button released...
					g_btn = 0;
					state = 0;
					if (btnTime < 6) {	// ignore short pulse
						g_btn = 0;
					}
					else if ((btnTime >= 6) && (btnTime <= 60)) {
						g_btn = 1;		// short press
					} else {
						g_btn = 2;		// long press
					}
				}
				break;
			default:
				state = 0;		   
		}
	}
}

/*
 * Display digit on 7 segment display
 */
void showDigit(void) {
	SEGM_A	= g_map[g_digit][1];
	SEGM_B	= g_map[g_digit][0];
	SEGM_C	= g_map[g_digit][5];
	SEGM_D	= g_map[g_digit][6];
	SEGM_E	= g_map[g_digit][7];
	SEGM_F	= g_map[g_digit][2];
	SEGM_G	= g_map[g_digit][3];
	SEGM_DP = g_dot;
}

/*
 * Initialize MCU
 */
void init(void) {
// Configure Oscillator
	OSCCON1 = 0x60;			// Osc. source = HFINTOSC, divider = 1
	OSCFRQ	= 0x05;			// HFINTOSC freq. = 16 MHz

// Initialize Interrupts
	INTCONbits.GIE = 1;		// Global interrupt enable
	
// Initialize I/O Ports
	ANSELA		= 0;		// Set PORTA to digital
	ANSELB		= 0;		// Set PORTB to digital
	ANSELC		= 0;		// Set PORTC to digital
	T_SEGMENTS	= OUTPUT;	// Output to 7 segment display (RC0..RC7)
	T_BUZZER	= OUTPUT;	// Buzzer output (RB0)
	T_OC1_OUT	= OUTPUT;	// open collector output 1 (RB5)
	T_OC2_OUT 	= OUTPUT;	// open collector output 2 (RB2)
	
	T_BUTTON 	= INPUT;	// Push-button input (RB1)
	T_INPUTS	= 0xFF;		// Protected inputs (RA0..RA7)		
	
	SEGMENTS	= 0xFF;		// Turn all segments off
	BUZZER		= 0;		// Turn buzzer off
	OC1_OUT		= 0;		// Turn off output 1
	OC2_OUT		= 0;		// Turn off output 2
	
// Initialize Timer 0
	T0CON0bits.T016BIT	= 0;		// 8 bit 
	T0CON0bits.T0OUTPS	= 0b0000;	// postscaler 1:1
	T0CON1bits.T0CS		= 0b010;	// fosc/4 clock
	T0CON1bits.T0CKPS	= 0b0000;	// prescaler 1:1
	T0CON0bits.T0EN		= 1;		// timer enable
	PIE0bits.TMR0IE		= 1;		// interrupt enable
}

/*
 * Annoying buzzer sound
 */
void buzzer(void) {
	BUZZER = 1;
	__delay_ms(25);
	BUZZER = 0;
	__delay_ms(40);
}

/*
 * Scanning the 8 input ports
 */
void scanInputs(void) {
	uint8_t n;
	
	for(n = 0; n < 8; n++) {
		if (~INPUS & (1<<n)) {
			if (n == 6) {			// Inputs 6 and 7 were swapped,
				g_alarms |= (1<<7);	// because reasons...
				g_digit = 7;
			} else if (n == 7) {
				g_alarms |= (1<<6);
				g_digit = 6;
			} else {
				g_alarms |= (1<<n);	// Inputs 0..5 g_map to RA0..RA5
				g_digit = n;
			}
		}
	}
}

/* 
 * LED and buzzer test
 */
void LEDtest(void) {
	uint8_t n;
	
	INTCONbits.GIE = 0;
	BUZZER = 1;
	for(n = 0; n <=7; n++) {
		SEGMENTS &= ~(1<<n);
		__delay_ms(250);
	}
	BUZZER = 0;
	INTCONbits.GIE = 1;
}

