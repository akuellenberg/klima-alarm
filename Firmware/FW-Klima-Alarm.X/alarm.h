/* 
 * File:   input.h
 * Author: ondre
 *
 * Created on June 11, 2021, 6:43 AM
 */

#ifndef INPUT_H
#define	INPUT_H

#ifdef	__cplusplus
extern "C" {
#endif
    
void initInputs(void);
void isrDebounceInputs(void);
uint8_t readKey(void);
uint8_t checkAlarm(void);
void clearAlarms(void);

#ifdef	__cplusplus
}
#endif

#endif	/* INPUT_H */

