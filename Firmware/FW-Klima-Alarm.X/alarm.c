#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "segment.h"
#include "alarm.h"

const uint8_t inputMap[8] = {7,6,5,4,3,2,0,1};

static volatile uint8_t debounce[9];
static volatile uint8_t btnTime = 0;
static volatile uint8_t btnState = 0;
static volatile uint8_t alarms[8];

void initInputs(void)
{
    for(uint8_t i = 0; i < 9; i++)
        debounce[i] = 0xff;
}

void clearAlarms(void)
{
    for (uint8_t i = 0; i < 8; i++)
        alarms[i] = 0;
}

uint8_t checkAlarm(void)
{
    for (uint8_t i = 0; i < 8; i++)
        if (alarms[i]) return 1;
    return 0;
}

void isrDebounceInputs(void)
{   
    for (uint8_t i = 0; i < 8; i++) {
        debounce[i] = (uint8_t)(debounce[i] << 1 ) | (((uint8_t)(PORTA >> i)) & 1);
        if (debounce[i] == 0)
            alarms[inputMap[i]] = 1;
    }
    
    debounce[8] = (uint8_t)(debounce[8] << 1 ) | PORTBbits.RB1;
    
    switch (btnState) {
        case 0: 
            if (debounce[8] == 0) {
                btnTime = 0;
                btnState = 1;
            }
            break;
        case 1:
            btnTime++;
            if (debounce[8] != 0)
                btnState = 0;
            break;
        case 2:
            if (debounce[8] != 0) {
                btnState = 0;
            }
            break;
        default:
            btnTime = 0;
            btnState = 0;
    }
}

uint8_t readKey(void) {
    if (btnState == 0) {
        if ((btnTime > 5) && (btnTime <= 50)) { 
            btnTime = 0;
            return 1;
        } else if (btnTime > 50) {
            btnTime = 0;
            return 2;
        }
    } else if (btnState == 1) {
        if (btnTime > 50) {
            btnState = 2;
            return 3;
        }
    }
    return 0;
}

void isrDisplayAlarms(void) {
    static uint8_t i = 0;

    if (i == 8)
        i = 0;
    while(i < 8) {
        if (alarms[i]) {
            setDigit(i + 1);
            i++;
            return;
        }
        i++;
    }        
}