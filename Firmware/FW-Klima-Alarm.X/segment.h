/* 
 * File:   segment.h
 * Author: ondre
 *
 * Created on June 8, 2021, 3:55 PM
 */

#ifndef SEGMENT_H
#define	SEGMENT_H

#ifdef	__cplusplus
extern "C" {
#endif
    
static uint8_t digit;

void setDigit(uint8_t d);
void isrBlinkingDot(void);
void isrDisplayAlarms(void);
void isrMutePattern(void);


#ifdef	__cplusplus
}
#endif

#endif	/* SEGMENT_H */

